import java.util.ArrayList;
import java.util.List;

public class SchoolManagement {
    private List <Student> studentList= new ArrayList<>();

    public void addStudentToList(Student student){
        studentList.add(student);

    }
    public void deleteStudentToList(Student student){
        studentList.remove(student);

    }

    List<Student> showStudentList() {

        for(int i=0; i<studentList.size(); i++) {
            System.out.println(studentList.get(i));
        }
        return studentList;
    }

    public static void main (String[] args) {

        SchoolManagement schoolManagement = new SchoolManagement();
        Student studentObj1 = new Student(1, "Madhuri", "Five");
        Student studentObj2 = new Student(2, "Ram", "UKG");
        Student studentObj3 = new Student(3, "Vinay", "LKG");

        schoolManagement.addStudentToList(studentObj1);
        schoolManagement.addStudentToList(studentObj2);
        schoolManagement.addStudentToList(studentObj3);
        schoolManagement.showStudentList();
        schoolManagement.deleteStudentToList(studentObj1);
        schoolManagement.showStudentList();
    }
}

